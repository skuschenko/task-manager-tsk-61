package com.tsc.skuschenko.tm.repository.dto;

import com.tsc.skuschenko.tm.dto.UserDTO;
import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import org.springframework.data.jpa.repository.Modifying;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;

import java.util.List;

public interface UserDTORepository extends AbstractDTORepository<UserDTO> {

    @Modifying
    @Query("DELETE FROM UserDTO e")
    void clear();

    @Query("SELECT e FROM UserDTO e")
    @Nullable List<UserDTO> findAllUser();

    @Query("SELECT e FROM UserDTO e WHERE e.email = :email")
    @Nullable UserDTO findByEmail(@Param("email") @NotNull String email);

    @Query("SELECT e FROM UserDTO e WHERE e.login = :login")
    @Nullable UserDTO findByLogin(@Param("login") @NotNull String login);

    @Query("SELECT e FROM UserDTO e WHERE e.id = :id")
    @Nullable UserDTO findUserById(@Param("id") @NotNull String id);

    @Modifying
    @Query("UPDATE UserDTO e SET e.isLocked=true WHERE e.login = :login")
    @NotNull UserDTO lockUserByLogin(@Param("login") @NotNull String login);

    @Modifying
    @Query("DELETE FROM UserDTO e WHERE e.login = :login ")
    void removeByLogin(@Param("login") @NotNull String login);

    @Modifying
    @Query("DELETE FROM UserDTO e WHERE e.id = :id")
    void removeOneById(@Param("id") @NotNull String id);

    @Modifying
    @Query("UPDATE UserDTO e SET e.passwordHash = :password WHERE " +
            "e.id = :userId")
    @NotNull UserDTO setPassword(
            @Param("userId") @NotNull String userId,
            @Param("password") @NotNull String password
    );

    @Modifying
    @Query("UPDATE UserDTO e SET e.isLocked=false WHERE e.login = :login")
    @NotNull UserDTO unlockUserByLogin(@Param("login") @NotNull String login);
}
