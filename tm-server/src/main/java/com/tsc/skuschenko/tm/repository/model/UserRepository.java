package com.tsc.skuschenko.tm.repository.model;

import com.tsc.skuschenko.tm.model.User;
import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import org.springframework.data.jpa.repository.Modifying;
import org.springframework.data.jpa.repository.Query;

import java.util.List;

public interface UserRepository extends AbstractRepository<User> {

    @Modifying
    @Query("DELETE FROM User e")
    void clear();

    @Query("SELECT e FROM User e ")
    @Nullable List<User> findAllUser();

    @Query("SELECT e FROM User e WHERE e.email = :email")
    @Nullable User findByEmail(@NotNull String email);

    @Query("SELECT e FROM User e WHERE e.login = :login")
    @Nullable User findByLogin(@NotNull String login);

    @Query("SELECT e FROM User e WHERE e.id = :id")
    @Nullable User findUserById(@NotNull String id);

    @Modifying
    @Query("UPDATE User e SET e.isLocked=true WHERE e.login = :login")
    @NotNull User lockUserByLogin(@NotNull String login);

    @Modifying
    @Query("DELETE FROM User e WHERE e.login = :login ")
    @Nullable User removeByLogin(@NotNull String login);

    @Modifying
    @Query("DELETE FROM User e WHERE e.id = :id")
    void removeOneById(@NotNull String id);

    @Modifying
    @Query("UPDATE User e SET e.passwordHash = :password WHERE " +
            "e.id = :userId")
    @NotNull User setPassword(@NotNull String userId, @NotNull String password);

    @Modifying
    @Query("UPDATE User e SET e.isLocked=false WHERE e.login = :login")
    @NotNull User unlockUserByLogin(@NotNull String login);

}
